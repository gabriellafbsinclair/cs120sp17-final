// Team
Names: Gabriella Sinclair, Kendrick Hougen
JHED IDs: gsincla2, khougen1

// Design
The design of this program adheres almost exactly to what was provided, with minimal extraneous additions. Check checking is accomplished by locating the enemy king on the board and then checking if friendly pieces can validly move to it. Checkmate checking looks for a check situation and then checks if any valid moves will escape from it.

The display board is largely unremarkable. We elected to use Unicode chess piece characters to represent each piece in black on a red and white board, with a clause to highlight both players' kings in yellow on each turn.

// Completeness
We are not aware of any missing functionality based on our testing.

// Special instructions
None on ugrad machines. May not compile on other machines if gcc version differs from ugrad due to use of regex.
