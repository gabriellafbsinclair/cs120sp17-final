

#include "Game.h"
#include "Prompts.h"
#include "Chess.h"


class TestMoves {

public:
    TestMoves();
    void testPawn();
    void testRook();
    void testKnight();
    void testBishop();
    void testQueen();
    void testKing();

private:
    ChessGame chess;
    
};
    
