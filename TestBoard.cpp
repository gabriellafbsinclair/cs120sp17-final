#include <assert.h>
#include "Game.h"
#include "Chess.h"
#include "TestBoard.h"


TestBoard::TestBoard() {}

void TestBoard::testMakeMoveStartIsEnd() {
    Position pos(2, 2);
    chess.initPiece(0,Player::WHITE,pos);
    int wrong = chess.makeMove(pos, pos);
    assert(wrong == -1);
    chess.m_pieces[chess.index(pos)] = nullptr;
}

void TestBoard::testMakeMoveOutOfBounds() {
    Position valid(2,3);
    Position ob(9,9);
    int wrong = chess.makeMove(valid, ob);
    assert(wrong == -7);
    wrong = chess.makeMove(ob, valid);
    assert(wrong == -7);
}

void TestBoard::testMakeMoveNoPiece() {
    Position start(2,3);
    Position end(4,5);
    chess.initPiece(0, Player::WHITE, end);
    int wrong = chess.makeMove(start, end);
    assert(wrong == -6);
    chess.m_pieces[chess.index(end)] = nullptr;
}

void TestBoard::testMakeMoveWrongColor() {
    Position start(2,3);
    Position end(4,5);
    chess.initPiece(0, Player::BLACK, start);
     int wrong = chess.makeMove(start, end);
    assert(wrong == -6);
    chess.m_pieces[chess.index(start)] = nullptr;
}

void TestBoard::testIsCheck() {
    Position pawn(2,2);
    Position king(3,3);
    chess.initPiece(0, Player::WHITE, pawn);
    chess.initPiece(5, Player::BLACK, king);
    bool check = chess.isCheck();
    assert(check);
    chess.m_pieces[chess.index(pawn)] = nullptr;
    chess.m_pieces[chess.index(king)] = nullptr;
}

void TestBoard::testCanCheckPawn() {
    Position pawn(2,2);
    Position push(2,3);
    Position king(3,3);
    chess.initPiece(0, Player::WHITE, pawn);
    chess.initPiece(5, Player::BLACK, king);
    Piece* pi = chess.getPiece(pawn);
    bool check = pi->canCheck(pawn, king, chess);
    assert(check);
    chess.makeMove(pawn, push);
    pi = chess.getPiece(push);
    check = pi->canCheck(push, king, chess);
    assert(check == false);
    chess.m_pieces[chess.index(push)] = nullptr;
    chess.m_pieces[chess.index(king)] = nullptr;
}

void TestBoard::testCanCheckRook() {
    Position rook(2,2);
    Position king(2,6);
    chess.initPiece(1, Player::BLACK, rook);
    chess.initPiece(5, Player::WHITE, king);
    Piece* pi = chess.getPiece(rook);
    bool check = pi->canCheck(rook, king, chess);
    assert(check);
    chess.m_pieces[chess.index(rook)] = nullptr;
    chess.m_pieces[chess.index(king)] = nullptr;
}

void TestBoard::testCanCheckKnight() {
    Position knight(2,2);
    Position king(4,3);
    chess.initPiece(2, Player::WHITE, knight);
    chess.initPiece(5, Player::BLACK, king);
    Piece* pi = chess.getPiece(knight);
    bool check = pi->canCheck(knight, king, chess);
    assert(check);
    chess.m_pieces[chess.index(knight)] = nullptr;
    chess.m_pieces[chess.index(king)] = nullptr;
}

void TestBoard::testCanCheckBishop() {
    Position bishop(2,2);
    Position king(5,5);
    chess.initPiece(3, Player::WHITE, bishop);
    chess.initPiece(5, Player::BLACK, king);
    Piece* pi = chess.getPiece(bishop);
    bool check = pi->canCheck(bishop, king, chess);
    assert(check);
    chess.m_pieces[chess.index(bishop)] = nullptr;
    chess.m_pieces[chess.index(king)] = nullptr;
}

void TestBoard::testCanCheckQueen() {
    Position queen(2,2);
    Position king(5,5);
    chess.initPiece(4, Player::BLACK, queen);
    chess.initPiece(5, Player::WHITE, king);
    Piece* pi = chess.getPiece(queen);
    bool check = pi->canCheck(queen, king, chess);
    assert(check);
    chess.m_pieces[chess.index(queen)] = nullptr;
    chess.m_pieces[chess.index(king)] = nullptr;
}

