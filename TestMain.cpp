#include <assert.h>
#include "Game.h"
#include "Chess.h"
#include "Prompts.h"
#include "TestBoard.h"
#include "TestMoves.h"


int main(void) {
    TestMoves tm;
    tm.testPawn();
    tm.testRook();
    tm.testKnight();
    tm.testBishop();
    tm.testQueen();
    tm.testKing();
    
    TestBoard tb;
    tb.testMakeMoveStartIsEnd();
    tb.testMakeMoveOutOfBounds();
    tb.testMakeMoveNoPiece();
    tb.testMakeMoveWrongColor();
    tb.testIsCheck();
    tb.testCanCheckPawn();
    tb.testCanCheckRook();
    tb.testCanCheckBishop();
    tb.testCanCheckKnight();
    tb.testCanCheckQueen();
    
    std::cout << "All tests passed!" << std::endl;
}
