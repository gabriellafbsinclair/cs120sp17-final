

#include <assert.h>
#include "Game.h"
#include "Chess.h"
#include "TestMoves.h"


TestMoves::TestMoves() {
}

void TestMoves::testPawn() {
    Position pos(2, 1);
    //4 possible moves
    Position m1(2,2);
    Position m2(3,2);
    Position m3(2,2);
    Position m4(2,3);
    Position illegal(5,5);
    
    chess.initPiece(0, Player::WHITE, pos);
    Piece* pawn = chess.getPiece(pos);
    
    assert(pawn->validMove(pos, m1, chess)==1);
    assert(pawn->validMove(pos, m2, chess)==1);
    assert(pawn->validMove(pos, m3, chess)==1);
    assert(pawn->validMove(pos, m4, chess)==1);
    assert(pawn->validMove(pos, illegal, chess)==-1);
    chess.m_pieces[chess.index(pos)] = nullptr;
}

void TestMoves::testRook(){
    Position start(2,2);
    chess.initPiece(1, Player::WHITE, start);
    Piece* rook = chess.getPiece(start);
    for (int i = 0; i < (int)chess.width(); i++) {
	Position possible(i, 2);
	if (i != 2) {
	    assert(rook->validMove(start, possible, chess) == 1);
	}
    }
    for (int j = 0; j < (int)chess.height(); j++) {
	Position possible(2, j);
	if (j != 2) {
	    assert(rook->validMove(start, possible, chess) == 1);
	}
    }
    Position illegal(3,4);
    assert(rook->validMove(start, illegal, chess) == -1);
    chess.m_pieces[chess.index(start)] = nullptr;
}

void TestMoves::testKnight(){
    Position start(3, 2);
    chess.initPiece(2, Player::WHITE, start);
    Piece* kn = chess.getPiece(start);
    //possible
    Position m1(5,3);
    Position m2(5,1);
    Position m3(1,3);
    Position m4(1,1);
    Position m5(2,0);
    Position m6(2,4);
    Position m7(4,0);
    Position m8(4,4);
    Position illegal(6,6);
    assert(kn->validMove(start, m1, chess)==1);
    assert(kn->validMove(start, m2, chess)==1);
    assert(kn->validMove(start, m3, chess)==1);
    assert(kn->validMove(start, m4, chess)==1);
    assert(kn->validMove(start, m5, chess)==1);
    assert(kn->validMove(start, m6, chess)==1);
    assert(kn->validMove(start, m7, chess)==1);
    assert(kn->validMove(start, m8, chess)==1);
    assert(kn->validMove(start, illegal, chess)==-1);
    chess.m_pieces[chess.index(start)] = nullptr;
}

void TestMoves::testBishop(){
    Position start(3, 2);
    chess.initPiece(3, Player::WHITE, start);
    Piece* bp = chess.getPiece(start);

    for (int i = 1; i < (int)chess.width(); i++) {
	Position diag(i, i-1);
	assert(bp->validMove(start, diag, chess)==1);
    }
    for(int i = 5; i > -1; i--) {
	Position diag(i,5-i);
	assert(bp->validMove(start, diag, chess)==1);
    }

    Position illegal(0,1);
    assert(bp->validMove(start, illegal, chess)==-1);
    chess.m_pieces[chess.index(start)] = nullptr;
}

void TestMoves::testQueen(){
    Position start(3, 2);
    chess.initPiece(4, Player::WHITE, start);
    Piece* qu = chess.getPiece(start);
    for (int i = 0; i < (int)chess.width(); i++) {
	Position possible(i, 2);
	assert(qu->validMove(start, possible, chess) == 1);
    }
    for (int j = 0; j < (int)chess.height(); j++) {
	Position possible(3, j);
	assert(qu->validMove(start, possible, chess) == 1);
    }
    for (int i = 1; i < (int)chess.width(); i++) {
	Position diag(i, i-1);
	assert(qu->validMove(start, diag, chess)==1);
    }
    for(int i = 5; i > -1; i--) {
	Position diag(i,5-i);
	assert(qu->validMove(start, diag, chess)==1);
    }
    Position illegal(0,1);
    assert(qu->validMove(start, illegal, chess)==-1);
    chess.m_pieces[chess.index(start)] = nullptr;
}

void TestMoves::testKing(){
    Position start(3, 2);
    chess.initPiece(5, Player::WHITE, start);
    Piece* k = chess.getPiece(start);
    for (int i = -1; i <= 1; i++) {
	for (int j = -1; j <= 1; j++) {
	    Position near(start.x+i, start.y+j);
	    if (near.x != start.x && near.y != start.y) {
		assert(k->validMove(start, near, chess)==1);
	    }
	}
    }
    Position illegal(1,5);
    assert(k->validMove(start, illegal, chess)==-1);
    chess.m_pieces[chess.index(start)] = nullptr;
}
