#include <assert.h>
#include <cctype>
#include <sstream>
#include <iostream>
#include <regex>
#include <string>
#include "Game.h"
#include "Prompts.h"
#include "Chess.h"
#include "Terminal.h"

///////////////
// Board //
///////////////

Board::~Board() {
    // Delete all pointer-based resources
    for (unsigned int i=0; i < m_width * m_height; i++)
        delete m_pieces[i];
    for (size_t i=0; i < m_registeredFactories.size(); i++)
        delete m_registeredFactories[i];
}

void Board::display() {
  Terminal::colorFg(false,(Terminal::Color)0);
  for(int i = 7; i >= 0; i--) {
    for(int j = 0; j < 8; j++) {
      Position pos (j,i);
      Player p;
      int id = -1;
      std::string piece = " ";
      if(getPiece(pos) != nullptr) {
	p = getPiece(pos)->owner();
	id = getPiece(pos)->id();
        if(p == BLACK) {
	  switch(id) {
	  case PAWN_ENUM:
	    piece = "\u265F";
	    break;
	  case ROOK_ENUM:
	    piece = "\u265C";
	    break;
	  case BISHOP_ENUM:
	    piece = "\u265D";
	    break;
	  case KNIGHT_ENUM:
	    piece = "\u265E";
	    break;
	  case QUEEN_ENUM:
	    piece = "\u265B";
	    break;
	  case KING_ENUM:
	    piece = "\u265A";
	    break;
	  }
	}
	else {
	  switch(id) {
	  case PAWN_ENUM:
	    piece = "\u2659";
	    break;
	  case ROOK_ENUM:
	    piece = "\u2656";
	    break;
	  case BISHOP_ENUM:
	    piece = "\u2657";
	    break;
	  case KNIGHT_ENUM:
	    piece = "\u2658";
	    break;
	  case QUEEN_ENUM:
	    piece = "\u2655";
	    break;
	  case KING_ENUM:
	    piece = "\u2654";
	    break;
	  }
	}
      }
      // Highlights kings in yellow
      if (id == KING_ENUM) {
	Terminal::colorBg((Terminal::Color)3);
      }
      // Light squares in white
      else if ((i % 2 && !(j % 2)) || (!(i % 2) && j % 2)) {
	Terminal::colorBg((Terminal::Color)7);
      }
      // Dark squares in red
      else {
	Terminal::colorBg((Terminal::Color)1);
      }
      std::cout << " " << piece << " ";
      id = -1;
      Terminal::colorBg((Terminal::Color)9);
    }
    std::cout << std::endl;
  }
  Terminal::set_default();
}

// Get the Piece at a specific Position, or nullptr if there is no
// Piece there or if out of bounds.
Piece* Board::getPiece(Position position) const {
  if (validPosition(position) && m_pieces[index(position)]) {
        return m_pieces[index(position)];
  }
  else if (!m_pieces[index(position)]) {
      return nullptr;
  }
  else {
      Prompts::outOfBounds();
      return nullptr;
  }
}

// Create a piece on the board using the factory.
// Returns true if the piece was successfully placed on the board
bool Board::initPiece(int id, Player owner, Position position) {
    Piece* piece = newPiece(id, owner);
    if (!piece) return false;

    // Fail if the position is out of bounds
    if (!validPosition(position)) {
        Prompts::outOfBounds();
        return false;
    }
    // Fail if the position is occupied
    if (getPiece(position)) {
        Prompts::blocked();
        return false;
    }
    m_pieces[index(position)] = piece;
    return true;
}

// Add a factory to the Board to enable producing
// a certain type of piece
bool Board::addFactory(AbstractPieceFactory* pGen) {
    // Temporary piece to get the ID
    Piece* p = pGen->newPiece(WHITE);
    int id = p->id();
    delete p;

    PieceGenMap::iterator it = m_registeredFactories.find(id);
    if (it == m_registeredFactories.end()) { // not found
        m_registeredFactories[id] = pGen;
        return true;
    } else {
        std::cout << "Id " << id << " already has a generator\n";
        return false;
    }
}

// Search the factories to find a factory that can translate `id' to
// a Piece, and use it to create the Piece. Returns nullptr if not found.
Piece* Board::newPiece(int id, Player owner) {
    PieceGenMap::iterator it = m_registeredFactories.find(id);
    if (it == m_registeredFactories.end()) { // not found
        std::cout << "Id " << id << " has no generator\n";
        return nullptr;
    } else {
        return it->second->newPiece(owner);
    }
}

int Board::makeMove (Position start,Position end)  {
    //out-of-bounds
    if (!validPosition(start) || !validPosition(end)) {
	Prompts::outOfBounds();
	return -7;
    }
    Piece* toMove = getPiece(start);
    Piece* moveTo = getPiece(end);
    //trying to take own player
    if (toMove == nullptr || toMove->owner() != playerTurn()) {
	Prompts::noPiece();
	return -6;
    }
    //start and end are the same position
    if (start.x == end.x && start.y == end.y) {
	Prompts::illegalMove();
	return -1;
    }
    int isMove = toMove->validMove(start,end,*this);
    if (isMove == -1) {
	Prompts::illegalMove();
	return -1;
    }
    if (isMove == -5 || (moveTo && moveTo->owner() == playerTurn())) {
	Prompts::blocked();
	return -5;
    }
    else {
        bool precheck = isCheck((Player)!playerTurn());
	m_pieces[index(start)] = nullptr;
	m_pieces[index(end)] = toMove;
	if(isCheck((Player)!playerTurn())) {
	  m_pieces[index(start)] = toMove;
	  m_pieces[index(end)] = moveTo;
	  if(precheck) {
	    Prompts::mustHandleCheck();
	    return -3;
	  }
	  else {
	    Prompts::cantExposeCheck();
	    return -2;
	  }
	}
	if(isCheck(playerTurn())) {
	  Prompts::check(playerTurn());
	}
	if (moveTo && moveTo->owner() == !playerTurn()) {
	    Prompts::capture(playerTurn());
	}
    }
    m_turn++;
    return 1;
}

Position Board::findKing(Player q) {
    Position kpos (0,0);
    for(std::vector<Piece*>::iterator it = m_pieces.begin();
	it != m_pieces.end(); it++) {
	if(*it != nullptr && (*it)->owner() == q &&
	   (*it)->id() == KING_ENUM) {
	    int kindex = it - m_pieces.begin();
	    int kx = kindex % 8;
	    int ky = (kindex - kx) / 8;
	    Position newkpos (kx,ky);
	    kpos = newkpos;
	}
    }
    return kpos;
}

bool Board::isCheck(Player p) {
    bool check = false;
    Position kpos = findKing((Player)(!p));
    for(std::vector<Piece*>::iterator it = m_pieces.begin();
	it != m_pieces.end() && !check; it++) {
	if(*it != nullptr && (*it)->owner() == p) {
	    int index = it - m_pieces.begin();
	    int x = index % 8;
	    int y = (index - x) / 8;
	    Position pos (x,y);
	    check = (*it)->canCheck(pos,kpos,*this);
	}
    }
    return check;
}

bool Board::checkmate() {
    bool checkmate = true;
    Position kpos = findKing((Player)!playerTurn());
    if(!isCheck(playerTurn())) {
      return false;
    }
    for(std::vector<Piece*>::iterator it = m_pieces.begin();
	it != m_pieces.end() && checkmate; it++) {
      if(*it != nullptr && (*it)->owner() != playerTurn()) {
	int index = it - m_pieces.begin();
	int x = index % 8;
	int y = (index - x)/8;
	Position from (x,y);
	for(int i = 0; i < 8; i++) {
	  for(int j = 0; j < 8; j++) {
	    Position to (i,j);
	    Piece* place = getPiece(to);
	    if(!validMove(from,to)) {
	      continue;
	    }
	    m_pieces[index(from)] = nullptr;
	    m_pieces[index(to)] = *it;
	    if(isCheck(playerTurn())) {
	      m_pieces[index(from)] = *it;
	      m_pieces[index(to)] = place;
	    }
	    if(!isCheck(playerTurn())) {
	      m_pieces[index(from)] = *it;
	      m_pieces[index(to)] = place;
	      checkmate = false;
	    }
	  }
	}
      }
    }
    return checkmate;
}


bool Board::isValidTurn(std::string userInput) {
    std::regex turnInput ("[a-zA-Z]\\d\\s[a-zA-z]\\d");
    return std::regex_match(userInput,turnInput);
}
