#undef FOR_RELEASE
#include <string>
#include <fstream>
#include <stdio.h>
#include <cmath>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <cctype>
#include "Game.h"
#include "Chess.h"
#include "Prompts.h"

// Make a move on the board. Return an int, with < 0 being failure
int ChessGame::makeMove(Position start, Position end) {
    int retCode = Board::makeMove(start, end);
    return retCode;
}

// Setup the chess board with its initial pieces
void ChessGame::setupBoard() {
    std::vector<int> pieces {
        ROOK_ENUM, KNIGHT_ENUM, BISHOP_ENUM, QUEEN_ENUM,
        KING_ENUM, BISHOP_ENUM, KNIGHT_ENUM, ROOK_ENUM
    };
    for (size_t i = 0; i < pieces.size(); ++i) {
        initPiece(PAWN_ENUM, WHITE, Position(i, 1));
        initPiece(pieces[i], WHITE, Position(i, 0));
        initPiece(pieces[i], BLACK, Position(i, 7));
        initPiece(PAWN_ENUM, BLACK, Position(i, 6));
    }
}

int ChessGame::fromFile() {
  Prompts::loadGame();
  std::string filename;
  std::getline(std::cin,filename);
  std::ifstream loadfile (filename);
  if(!loadfile.is_open()) {
    Prompts::loadFailure();
    return 0;
  }
  std::string tag;
  loadfile >> tag;
  if(tag.compare("chess")) {
    Prompts::loadFailure();
    return 0;
  }
  int turn = 1;
  loadfile >> turn;
  m_turn = turn;
  loadfile.get();
  std::string info;
  std::getline(loadfile,info);
  while(!info.empty()) {
    std::stringstream readinfo (info);
    int p,id;
    std::string pos;
    readinfo >> p >> pos >> id;
    initPiece(id,(Player)p,Position(tolower(pos[0]) - 'a',pos[1] - '0' - 1));
    std::getline(loadfile,info);
  }
  loadfile.close();
  return 1;
}

int ChessGame::save() {
  Prompts::saveGame();
  std::string filename;
  std::getline(std::cin, filename);
  std::ofstream savefile (filename);
  if(!savefile.is_open()) {
    Prompts::saveFailure();
    return 0;
  }
  savefile << "chess\n";
  savefile << m_turn << "\n";
  for(std::vector<Piece*>::iterator it = m_pieces.begin();
      it != m_pieces.end(); it++) {
    int index = it - m_pieces.begin();
    int x = index % 8;
    int y = (index - x) / 8;
    char a = x + 'a';
    char b = y + '0' + 1;
    if(*it != nullptr) {
      savefile << (int)(*it)->owner() << " " << a << b << " " << (*it)->id() << "\n";
    }
  }
  savefile.close();
  return 1;
}

int Pawn::validMove (Position start, Position end, const Board& board) const {
    Player p = board.playerTurn();
    bool moveOkay = false;

    //4 Movements:
    //if pawn has not moved, can move 2 spaces
    //can always move one space in y direction depending upon color
    //can capture on diagonal
    if (p == WHITE) {
	if (start.y == 1) {
	    moveOkay = (end.x == start.x && start.y + 2 == end.y) ||
		(end.x == start.x && start.y + 1 == end.y) ||
		(end.x == (start.x + 1) && start.y + 1 == end.y) ||
		(end.x == (start.x - 1) && start.y + 1 == end.y) ? true : false;
	    
	} else {
	    moveOkay = (end.x == start.x && start.y + 1 == end.y) ||
		(end.x == (start.x + 1) && start.y + 1 == end.y) ||
		(end.x == (start.x - 1) && start.y + 1 == end.y) ? true : false;
	}
    } else if (p == BLACK) {
	if (start.y == 6) {
	    moveOkay = (end.x == start.x && start.y - 2 == end.y) ||
		(end.x == start.x && start.y - 1 == end.y) ||
		(end.x == (start.x + 1) && start.y - 1 == end.y) ||
		(end.x == (start.x - 1) && start.y - 1 == end.y) ? true : false;

	} else {
	    moveOkay = (end.x == start.x && start.y - 1 == end.y) ||
		(end.x == (start.x + 1) && start.y - 1 == end.y) ||
		(end.x == (start.x - 1) && start.y - 1 == end.y) ? true : false;
	}
    }

    if (!moveOkay) { //not one of the 4 move options
	return -1; //MOVE_ERROR_ILLEGAL
    }

    bool blockOkay = true;
    if (p == WHITE) {
	for (unsigned int i = start.y + 1; i <= end.y && blockOkay; i++) {
	    Position j (start.x,i);
	    blockOkay = board.getPiece(j) ? false : true;
	}
    }
    else if (p == BLACK) {
	for (unsigned int i = start.y - 1; i >= end.y && blockOkay; i--) {
	    Position j (start.x,i);
	    blockOkay = board.getPiece(j) ? false : true;
	}
    }
    if (!blockOkay) {
	return -5;
    }
    return 1;
}

bool Pawn::canCheck(Position pos,Position kpos,const Board& board) const {
  Position move1 (pos.x + 1,pos.y + 1);
  Position move2 (pos.x - 1,pos.y + 1);
  if((move1.x == kpos.x && move1.y == kpos.y) ||
     (move2.x == kpos.x && move2.y == kpos.y)) {
    return true;
  }
  else {
    return false;
  }
}

int Rook::validMove(Position start, Position end, const Board& board) const {
    bool moveOkay = (start.x == end.x) ^ (start.y == end.y) ? true : false;
    if (!moveOkay) {
	return -1; //MOVE_ERROR_ILLEGAL
    }
    
    bool blockOkay = true;
    int p1,p2,q;
    if (start.x == end.x && end.y > start.y) {
	p1 = end.y;
	p2 = start.y;
	q = start.x;
    }
    else if (start.x == end.x && end.y < start.y) {
	p1 = start.y;
	p2 = end.y;
	q = start.x;
    }
    else if (start.y == end.y && start.x < end.x) {
	p1 = start.x;
	p2 = end.x;
	q = start.y;
    }
    else if (start.y == end.y && start.x > end.x) {
	p1 = end.y;
	p2 = start.y;
	q = start.y;
    }
    for(int i = p1 + 1; i < p2 && blockOkay; i++) {
	Position j (i,q);
	Position k (q,i);
	Position pos = start.x == end.x ? k : j;
	blockOkay = !board.getPiece(pos);
    }

    if (!blockOkay) {
	return -5;
    }
    return 1;
}

bool Rook::canCheck(Position pos,Position kpos,const Board& board) const {
    bool check = false;
    if(validMove(pos,kpos,board) >= 0) {
	check = true;
    }
    return check;
}

int Knight::validMove (Position start, Position end,
		       const Board& board) const {
  int xDist = end.x - start.x;
  int yDist = end.y - start.y;
  // Can't use pow() oeprator due to overflow with negative ints
  int sqrDist = xDist*xDist + yDist*yDist;
    if (sqrDist != 5) {
	return -1; //MOVE_ERROR_ILLEGAL
    }
    return 1;
}

bool Knight::canCheck(Position pos,Position kpos,const Board& board) const {
    bool check = false;
    if(validMove(pos,kpos,board) >= 0) {
	check = true;
    }
    return check;
}

int Bishop::validMove (Position start, Position end,
		       const Board& board) const {
    int delx = end.x - start.x;
    int dely = end.y - start.y;
    bool moveOkay = (delx == dely || delx == -dely) ? true : false;
    if (!moveOkay) {
	return -1;
    }

    bool blockOkay = true;
    if (end.x > start.x) {
	for(unsigned int i = start.x + 1; i < end.x && blockOkay; i++) {
	    Position j (i,start.y - start.x + i);
	    blockOkay = board.getPiece(j) ? false : true;
	}
    }
    else if (end.x < start.x) {
	for(unsigned int i = end.x + 1; i < start.x && blockOkay; i++) {
	    Position j (i,end.y - end.x + i);
	    blockOkay = board.getPiece(j) ? false : true;
	}
    }
    if (!blockOkay) {
	return -5;
    }
    return 1;
}

bool Bishop::canCheck(Position pos,Position kpos,const Board& board) const {
    bool check = false;
    if(validMove(pos,kpos,board) >= 0) {
	check = true;
    }
    return check;
}

int Queen::validMove (Position start, Position end, const Board& board) const {
    int delx = end.x - start.x;
    int dely = end.y - start.y;
    bool moveOkay = ((start.x == end.x) ^ (start.y == end.y)) ||
		     (delx == dely || delx == -dely) ? true : false;
    if (!moveOkay) {
	return -1;
    }

    bool blockOkay = true;
    if((start.x == end.x) ^ (start.y == end.y)) {
	int p1,p2,q;
	if (start.x == end.x && end.y > start.y) {
	    p1 = start.y;
	    p2 = end.y;
	    q = start.x;
	}
	else if (start.x == end.x && end.y < start.y) {
	    p1 = end.y;
	    p2 = start.y;
	    q = start.x;
	}
	else if (start.y == end.y && start.x < end.x) {
	    p1 = start.x;
	    p2 = end.x;
	    q = start.y;
	}
	else if (start.y == end.y && start.x > end.x) {
	    p1 = end.y;
	    p2 = start.y;
	    q = start.y;
	}
	for(int i = p1 + 1; i < p2 && blockOkay; i++) {
	    Position j (i,q);
	    Position k (q,i);
	    Position pos = start.x == end.x ? k : j;
	    blockOkay = !board.getPiece(pos);
	}
    }
    else if(end.y - start.y == end.x - start.x ||
	    end.y - start.y == -(end.x - start.x)) {
	if (end.x > start.x) {
	    if (end.y > start.y) {
		for(unsigned int i = start.x + 1; i < end.x && blockOkay; i++) {
		    Position j (i,start.y - start.x + i);
		    blockOkay = !board.getPiece(j);
		}
	    } else if (end.y < start.y) {
		for(unsigned int i = start.x + 1; i < end.x && blockOkay; i++) {
		    Position j (i,start.y + start.x - i);
		    blockOkay = !board.getPiece(j);
		}
	    }
	}
	else if (end.x < start.x) {
	    if (end.y > start.y) {
		for(unsigned int i = end.x + 1; i < start.x && blockOkay; i++) {
		    Position j (i,end.y + end.x - i);
		    blockOkay = !board.getPiece(j);
		}
	    } else if (end.y < start.y) {
		for(unsigned int i = end.x + 1; i < start.x && blockOkay; i++) {
		    Position j (i,end.y - end.x + i);
		    blockOkay = !board.getPiece(j);
		}
	    }
	}
    }
    if (!blockOkay) {
	return -5;
    }
    return 1;
}

bool Queen::canCheck(Position pos,Position kpos,const Board& board) const {
    bool check = false;
    if(validMove(pos,kpos,board) >= 0) {
	check = true;
    }
    return check;
}

int King::validMove (Position start, Position end, const Board& board) const {
    int xDist = end.x - start.x;
    int yDist = end.y - start.y;
    bool moveOkay = (xDist == -1 || xDist == 0 || xDist == 1) &&
	(yDist == -1 || yDist == 0 || yDist == 1) ? true : false;
    if (!moveOkay) {
	return -1;
    }
    bool blockOkay = true;
    Piece* p = board.getPiece(end);
    if (p != nullptr && p->owner() == board.playerTurn()) {
	blockOkay = false;
    }

    if (!blockOkay) {
	return -5;
    }
    return 1;
}

bool King::canCheck(Position pos,Position kpos,const Board& board) const {
    bool check = false;
    if(validMove(pos,kpos,board) >= 0) {
	check = true;
    }
    return check;
}
