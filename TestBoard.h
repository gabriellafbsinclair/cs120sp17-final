

#include "Game.h"
#include "Prompts.h"
#include "Chess.h"


class TestBoard {

public:
    TestBoard();
    void testMakeMoveStartIsEnd();
    void testMakeMoveOutOfBounds();
    void testMakeMoveNoPiece();
    void testMakeMoveWrongColor();
    void testIsCheck();
    void testCanCheckPawn();
    void testCanCheckRook();
    void testCanCheckKnight();
    void testCanCheckBishop();
    void testCanCheckQueen();

    ChessGame chess;
private:
    
    
};
    
