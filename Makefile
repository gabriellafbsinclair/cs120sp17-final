CXX = g++
CXXFLAGS = -Wall -Wextra -pedantic -std=c++11 -g

chess: main.o Chess.o Board.o
	$(CXX) $(CXXFLAGS) main.o Chess.o Board.o -o chess
main.o: main.cpp Game.h Chess.h Prompts.h
	$(CXX) $(CXXFLAGS) -c main.cpp
Board.o: Board.cpp Game.h Chess.h Prompts.h
	$(CXX) $(CXXFLAGS) -c Board.cpp
Chess.o: Chess.cpp Game.h Chess.h Prompts.h
	$(CXX) $(CXXFLAGS) -c Chess.cpp
test: TestMain.o TestBoard.o Chess.o Board.o TestMoves.o
	$(CXX) $(CXXFLAGS) TestMain.o TestMoves.o TestBoard.o Chess.o Board.o -o test
TestMain.o: TestMain.cpp Game.h Chess.h Prompts.h TestBoard.h TestMoves.h
	$(CXX) $(CXXFLAGS) -c TestMain.cpp
TestBoard.o: TestBoard.cpp Game.h Chess.h Prompts.h TestBoard.h
	$(CXX) $(CXXFLAGS) -c TestBoard.cpp
TestMoves.o: TestMoves.cpp Game.h Chess.h Prompts.h TestMoves.h
	$(CXX) $(CXXFLAGS) -c TestMoves.cpp
clean:
	rm *.o chess

