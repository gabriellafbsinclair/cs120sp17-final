#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <cctype>
#include <utility>
#include <sstream>
#include "Game.h"
#include "Chess.h"
#include "Prompts.h"

int main() {
  ChessGame chess;
  bool disp = false;
  Prompts::menu();
  std::string userInput;
  getline(std::cin,userInput);
  while (userInput != "1" && userInput != "2") {
    Prompts::menu();
    getline(std::cin,userInput);
  }
  if (userInput == "1") {
    chess.setupBoard();
  }
  else if (userInput == "2") {
    chess.fromFile();
  }
  bool cm = false;
  bool sm = false;
  while(!cm && !sm) {
    int moveMade = 0;
    std::string userInput;
    while(moveMade != 1) {
      Prompts::playerPrompt(chess.playerTurn(),chess.turn());
      std::getline(std::cin,userInput);
      for(unsigned int i = 0; i < userInput.length(); i++) {
	userInput[i] = tolower(userInput[i]);
      }
      if(!userInput.compare("q")) {
	return 0;
      }
      else if(!userInput.compare("board")) {
	moveMade = 0;
	chess.display();
	disp = !disp;
      }
      else if(!userInput.compare("forfeit")) {
	Prompts::win((Player)(!chess.playerTurn()),chess.turn());
	Prompts::gameOver();
	return 0;
      }
      else if(!userInput.compare("save")) {
	chess.save();
      }
      else if(chess.isValidTurn(userInput)) {
	std::stringstream readMove (userInput);
	std::string sstart,send;
	readMove >> sstart >> send;
	Position start (tolower(sstart[0]) - 'a',sstart[1] - '0' - 1);
	Position end (tolower(send[0]) - 'a',send[1] - '0' - 1);
        moveMade = chess.makeMove(start,end);
      }
      else {
	moveMade = 0;
	Prompts::parseError();
      }
    }
    if(disp) {
      chess.display();
    }
    cm = chess.checkmate();
    sm = chess.stalemate();
  }
  if(cm) {
    Prompts::checkMate(chess.playerTurn());
    Prompts::win(chess.playerTurn(),chess.turn());
  }
  else if(sm) {
    Prompts::staleMate();
  }
  Prompts::gameOver();
  return 0; 
}
